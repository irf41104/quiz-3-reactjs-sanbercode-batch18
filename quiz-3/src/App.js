import React from 'react';
import './App.css';
import Routes from './quiz/Routes'
import { LoginProvider } from './quiz/LoginContext';

function App() {
  return (
    <LoginProvider>
      <Routes/>  
    </LoginProvider>
  );
}

export default App;
