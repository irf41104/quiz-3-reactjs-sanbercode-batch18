import React, {useContext} from "react"
import {MovieContext} from "./Context"
import axios from 'axios'
import "./Movie.css"

const MovieList = () =>{
    const [movieData, setMovieData, input, setInput] = useContext(MovieContext)

    const handleDelete = (event) => {
        let idMovieData = parseInt(event.target.value)
    
        let newMovieData = movieData.filter(el => el.id !== idMovieData)
    
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idMovieData}`)
        .then(res => {
          console.log(res)
        })
              
        setMovieData([...newMovieData])
        
      }
      
      const handleEdit = (event) =>{
        let idMovieData = parseInt(event.target.value)
        let isiMovie = movieData.find(x=> x.id === idMovieData)
        setInput({title: isiMovie.title, description: isiMovie.description, year: isiMovie.year, duration: isiMovie.duration, genre: isiMovie.genre, rating: isiMovie.rating, image_url: isiMovie.image_url, id: idMovieData})
      }

    return(
        <>
          <h1>Daftar Film</h1>
          <table>
            <thead>
              <tr>
                <th>No</th>
                <th>Title</th>
                <th>Description</th>
                <th>Year</th>
                <th>Duration</th>
                <th>Genre</th>
                <th>Rating</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
    
                {
                  movieData !== null && movieData.map((item, index)=>{
                    let desc ;
                    if(item.description===null){
                      desc=null
                    }
                    else desc= [...item.description.substr(0, 25),..."..."]
                    let titl = [...item.title.substr(0, 25),..."..."];
                    return(                    
                      <tr key={index}>
                        <td>{index+1}</td>
                        <td>{titl}</td>
                        <td>{desc}</td>
                        <td>{item.year}</td>
                        <td>{item.duration}</td>
                        <td>{item.genre}</td>
                        <td>{item.rating}</td>
                        <td>
                          <button style = {{width: "60px"}} onClick={handleEdit} value={item.id}>Edit</button>
                          &nbsp;
                          <button style = {{width: "60px"}} onClick={handleDelete} value={item.id}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>
        </>
    )
}

export default MovieList
