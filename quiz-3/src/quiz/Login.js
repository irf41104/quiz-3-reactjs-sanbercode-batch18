import React, {useContext} from 'react'
import {LoginContext} from "./LoginContext";
import '../css/style.css'

function Login(){
    const [login, setLogin, input, setInput] = useContext(LoginContext) 
    
    const handleSubmit = (event) =>{
        event.preventDefault()
    
        let username = input.username
        let password = input.password
    
        if (login.username === username && login.password === password){       
              setLogin({enableEdit : true,logout : true})
              alert("Login Success");
        }
        else {
            setInput({username : '',password : '',enableEdit : false, logout:false})
            alert("Username atau Password Salah");
        }
    
    }

    const handleChange = (event) =>{
        let typeOfInput = event.target.name
    
        switch (typeOfInput){
          case "username":
          {
            setInput({...input, username: event.target.value});
            break
          }
          case "password":
          {
            setInput({...input, password: event.target.value});
            break
          }
        default:
          {break;}
        }
      }

    return(
        <>
            <div class = "logbox">
                <div class ='loginform'>
                    <h2 style={{textAlign: "center"}}>Login</h2>
                </div>
                <form class='dataform' onSubmit={handleSubmit}>
                    <label style={{paddingTop:'13px'}}>
                        Username
                    </label>
                    <input class="inpt" type="text" required name="username" value={input.title} onChange={handleChange}/>
                    <div class="inptborder"></div>
                    <label style={{paddingTop:'13px'}}>
                        Password
                    </label>
                    <input class="inpt" type="password" required name="password" style={{marginTop: "10px", marginLeft: "4px"}} value={input.title} onChange={handleChange}/>
                    <div class="inptborder"></div>
                    <br/>
                    <button style={{marginLeft: "184px", height: "30px", borderRadius: "6px", backgroundColor:"whitesmoke", border: "2px solid grey"}}>Login</button>
                </form>
            </div>
        </>
    )
}

export default Login
