import React from "react";
import "../css/style.css";

const About = () => {
    
    return (
        <section>
          <div style={{padding: "10px", border: "1px solid #ccc"}}>
            <h1 style={{textAlign: "center"}}>Data Peserta Sanbercode Bootcamp Reactjs</h1>
            <ol>
              <li><strong style={{"width": "100px"}}>Nama:</strong>Irfanda Mahardhika</li> 
              <li><strong style={{"width": "100px"}}>Email:</strong>irfandamhk@gmail.com</li> 
              <li><strong style={{"width": "100px"}}>Sistem Operasi yang digunakan:</strong>MacOS</li>
              <li><strong style={{"width": "100px"}}>Akun Gitlab:</strong>@irf41104</li> 
              <li><strong style={{"width": "100px"}}>Akun Telegram:</strong>+629112409197</li> 
            </ol>
          </div>
          <br/>
          <br/>
          </section>
    )
}

export default About;
