import React from "react";
import { 
    BrowserRouter as Router,
    Switch, 
    Link, 
    Route } from "react-router-dom";
import Index from "./Index";
import About from "./About";
import Contact from "./Contact";
import "../css/style.css";
import logo from '../img/logo.png';

const Navbar = () => {
  return (
    <>
    <Router>
      <header>
        <img Id="logo" src={logo} width={200} alt="logo" />
        <nav>
          <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/about">About</Link></li>
            <li><Link to="/contact">Contact</Link></li>
          </ul>
        </nav>
      </header>
      <Switch>
        <Route exact path="/" component={Index}></Route>
        <Route path="/about" component={About} />
        <Route path="/contact" component={Contact} />
      </Switch>
      </Router>
    </>
  );
}

export default Navbar;