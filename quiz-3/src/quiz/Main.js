import React from "react"
import {MovieProvider} from "./Context"
import MovieList from "./List"
import MovieForm from "./Form"

const MovieListEditor = () =>{
    return(
      <MovieProvider>
          <MovieList/>
          <br/>
          <MovieForm/>
      </MovieProvider>
    )
  }
  
  export default MovieListEditor
