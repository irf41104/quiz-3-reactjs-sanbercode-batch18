import React from "react";

const About = () => {
  return (
    <section>
      <div style={{ padding: 10, border: "1px solid #ccc" }}>
        <h1 style={{ textAlign: "center" }}>
          Data Peserta Sanbercode Bootcamp Reactjs
        </h1>
        <ol style = {{paddingLeft: "50px"}}>
          <li>
            <strong style={{ width: 100 }}>Nama: </strong>Irfanda Mahardhika
          </li>
          <li>
            <strong style={{ width: 100 }}>Email: </strong>irfandamhk@gmail.com
          </li>
          <li>
            <strong style={{ width: 100 }}>
              Sistem Operasi yang digunakan:
            </strong> Mac OS
          </li>
          <li>
            <strong style={{ width: 100 }}>Akun Gitlab: </strong>@irf41104
          </li>
          <li>
            <strong style={{ width: 100 }}>Akun Telegram: </strong>+628112409197
          </li>
        </ol>
      </div>
    </section>
  );
};

export default About;
